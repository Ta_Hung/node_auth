const mongose = require('mongoose');

const Role = mongose.model(
  'Role',
  new mongose.Schema({
    name: String,
  })
);

module.exports = Role;
