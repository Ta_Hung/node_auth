/* Express is for building the Rest apis
body-parser helps to parse the request and create the req.body object
cors provides Express middleware to enable CORS */
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dbConfig = require('./config/db.config');

const app = express();

let corsOptions = {
  origin: 'http://localhost:8081',
};

//parse request of content-type application/json
app.use(cors(corsOptions));

app.use(express.json());

//parse request of content-type application/x-ww-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const db = require('./models');
const Role = db.role;

db.mongoose
  .connect(`mongodb://${dbConfig.HOST}:${dbConfig.PORT}/${dbConfig.DB}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('Successfully connected to MongoDB.');
    initial();
  })
  .catch((err) => {
    console.error('Connection error ', err);
    process.exit();
  });

//define a GET route which is simple for test.
app.get('/', (req, res) => {
  res.json({ message: 'Welcome Bounty Hunter' });
});

//routes
require('./routes/auth.routes')(app);
require('./routes/user.routes')(app);

//listen on port 8080 for incoming requests.
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is runnning on port ${PORT}.`);
});

function initial() {
  //Returns the count of all documents in a collection or view. Creates new roles if empty rows
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({ name: 'user' }).save((err) => {
        if (err) {
          console.log('error', err);
        }
        console.log("added 'user' to roles collection");
      });

      new Role({ name: 'moderator' }).save((err) => {
        if (err) {
          console.log('error', err);
        }
        console.log("added 'moderator' to roles collection");
      });

      new Role({ user: 'admin' }).save((err) => {
        if (err) {
          console.log('error', err);
        }
        console.log("added 'admin' to roles collection");
      });
    }
  });
}
