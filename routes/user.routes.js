const { authJwt } = require('../middlewares');
const controller = require('../controllers/user.controller');

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept'
    );
    next();
  });
  //GET /api/test/all
  app.get('/api/test/all', controller.allAccess);
  //GET /api/test/user for loggedin users (user/moderator/admin)
  app.get('/api/test/user', [authJwt.verifyToken], controller.userBoard);
  ///api/test/mod for moderator
  app.get(
    '/api/test/mod',
    [authJwt.verifyToken, authJwt.isModerator],
    controller.moderatorBoard
  );
  ///api/test/admin for admin
  app.get(
    '/api/test/admin',
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.adminBoard
  );
};
